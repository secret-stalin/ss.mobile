/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from 'react';
import {createAppContainer, createStackNavigator} from "react-navigation";
import LoginScreen from "./app/components/LoginScreen";
import MenuScreen from "./app/components/MenuScreen";
import SplashScreen from "./app/components/SplashScreen";
import GameChatScreen from "./app/components/GameChatScreen";
import GameScreen from "./app/components/GameScreen";
import CreateGameScreen from "./app/components/CreateGameScreen";
import LobbyScreen from "./app/components/LobbyScreen";
import PublicLobbyScreen from "./app/components/PublicLobbyScreen";
import firebase from 'react-native-firebase';
import {AsyncStorage} from 'react-native';
import {setFireBaseToken} from "./app/services/UserService";
import type {Notification, NotificationOpen} from 'react-native-firebase';
import {getGameChats} from "./app/services/GameChatService";
import WinScreen from "./app/components/WinScreen";
import {GameOverviewScreen} from "./app/components/GameOverviewScreen";

const AppNavigator = createStackNavigator(
    {
        Login: {
            screen: LoginScreen,
            navigationOptions: {
                header: null
            }
        },
        Menu: {
            screen: MenuScreen,
            navigationOptions: {
                header: null
            }
        },
        GameChat: {
            screen: GameChatScreen,
        },
        Game: {
            screen: GameScreen,
        },
        GameOverview: {
            screen: GameOverviewScreen,
        },
        CreateGame: {
            screen: CreateGameScreen,
        },
        Lobby: {
            screen: LobbyScreen,
        },
        PublicLobbies: {
            screen: PublicLobbyScreen,
        },
        Splash: {
            screen: SplashScreen,
            navigationOptions: {
                header: null
            }
        },
        WinScreen: {
            screen: WinScreen,
            navigationOptions: {
              header: null
           }
        }
    },
    {
        initialRouteName: 'Splash'
    });

const AppContainer = createAppContainer(AppNavigator);

type Props = {};

export default class App extends Component<Props> {
    fcmToken: string;

    async componentDidMount() {
        this.checkPermission();

        firebase.messaging().subscribeToTopic("/topics/game1");

        const notificationOpen: NotificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            // App was opened by a notification
            // Get the action triggered by the notification being opened
            const action = notificationOpen.action;
            // Get information about the notification that was opened
            const notification: Notification = notificationOpen.notification;
            switch (notification.data.TYPE) {
                case "FRIEND_REQUEST":
                    console.log("Friend request");
                    break;
                case "INVITE":
                    console.log("Invite");
                    break;
                case "YOUR_TURN":
                    console.log("Your turn");
                    //this.navigateToGame(notification.data.TARGETID);
                    break;
                case "ENDPHASE":
                    console.log("End Phase");
                    //this.navigateToGame(notification.data.TARGETID);
                    break;
            }
        }
    }

    async navigateToGame(gameId: number) {
        let gamechats = await getGameChats(gameId);
        this.navigator &&
        this.navigator.dispatch(
            NavigationActions.navigate('Game', {chats: gamechats})
        );
    }

    //1
    async checkPermission() {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            //this.getToken();
        } else {
            this.requestPermission();
        }
    }

    //3
    async getToken() {
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken();
            if (fcmToken) {
                // user has a device token
                await AsyncStorage.setItem('fcmToken', fcmToken);
                setFireBaseToken(fcmToken);
            }
        }
    }

    //2
    async requestPermission() {
        try {
            await firebase.messaging().requestPermission();
            // User has authorised
            this.getToken();
        } catch (error) {
            // User has rejected permissions
        }
    }

    render() {
        return (
            <AppContainer/>
        );
    }
}


