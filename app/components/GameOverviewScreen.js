import {Component} from "react";
import {ScrollView} from "react-native";
import {getGames} from "../services/GameService";
import React from "react";
import {Card, Icon, ListItem} from "react-native-elements";
import TouchableScale from "react-native-touchable-scale";
import {getGameChats} from "../services/GameChatService";


export class GameOverviewScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            games: []
        };
        getGames().then(value => {
            this.setState({games: value})
        })
    }

    render() {
        return (
            <ScrollView>
                <Card title="Your games">
                    {
                        this.state.games.map((game, i) => {
                            return (
                                <ListItem
                                    key={i}
                                    onPress={() => this.navigateToGame(game.gameId)}
                                    Component={TouchableScale}
                                    friction={90}
                                    tension={100}
                                    activeScale={0.95}
                                    title={game.name}
                                    titleStyle={{color: 'black', fontWeight: 'bold'}}
                                    subtitleStyle={{color: 'black'}}
                                    subtitle={ game.active ? 'Role to vote: ' + game.roleTypeToVote : 'Winning role: ' + game.winningRole}
                                    chevron={<Icon
                                        name="chevron-right"/>}
                                />
                            );
                        })
                    }
                </Card>
            </ScrollView>
        )
    }

    async navigateToGame(gameId: number) {
        let gamechats = await getGameChats(gameId);
        this.props.navigation.navigate('Game', {gameId: gameId, chats: gamechats});
    }

}
