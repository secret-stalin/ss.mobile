import React, {Component} from 'react';
import {Button, StyleSheet, Text, View, AsyncStorage, Alert} from "react-native";

export default class MenuScreen extends Component {
    async handleSubmit() {
        await AsyncStorage.removeItem('jwt');
        this.props.navigation.navigate('Login');
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.header}>You Are Logged In</Text>
                <Button
                    title="Logout"
                    onPress={this.handleSubmit.bind(this)}/>
                <Button
                    title="Games"
                    onPress={() => this.props.navigation.navigate('GameOverview')}/>
                <Button
                    title="Start Game"
                    onPress={() => this.props.navigation.navigate('CreateGame')}/>
                <Button
                    title="Join public lobby"
                    onPress={() => this.props.navigation.navigate('PublicLobbies')}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 40,
        marginBottom: 50,
        fontWeight: 'bold',
    },
    label: {
        fontSize: 24,
        marginBottom: 5,
        marginTop: 10,
        flexDirection: 'column',
        alignItems: 'flex-start',
    },
    button: {
        width: 350,
        height: 55,
        backgroundColor: '#42A5F5',
        margin: 10,
        padding: 8,
        color: 'white',
        borderRadius: 14,
        fontSize: 18,
        fontWeight: '500',
    },
    container: {
        justifyContent: 'center',
        marginTop: 50,
        padding: 20,
        backgroundColor: '#ffffff',
    }
});
