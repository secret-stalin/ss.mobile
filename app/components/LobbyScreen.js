import React, {Component} from 'react';
import {View, Button, ScrollView} from "react-native";
import {startGame} from "../services/GameService";
import {Divider, ListItem, Text} from "react-native-elements";
import SockJsClient from 'react-stomp';
import {getGameChats} from "../services/GameChatService";
import {decodeJwt, getUserFriends} from "../services/UserService";
import {sendInvite} from "../services/LobbyService";


export default class LobbyScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            lobby: props.navigation.getParam('lobby'),
            jwtToken: props.navigation.getParam('jwtToken'),
            serverUrl: 'https://secret-stalin-backend.herokuapp.com/socket?access_token=' + props.navigation.getParam('jwtToken'),
            friends: [],
            currentMail: ''
        };
        decodeJwt().then(res => {
            this.setState({currentMail: res.user_name});
            getUserFriends(res.user_name).then(friendsResult => {
                this.setState({friends: friendsResult})
            });
        })
    }

    inviteFriend(email) {
        sendInvite(this.state.lobby.lobbyId, email).then()
    }

    handleStartGame() {
        startGame(this.state.lobby.lobbyId)
    }

    onMessageReceive = async (msg, topic) => {
        if (topic.toString().startsWith("/lobby/start/")) {
            let gamechats = await getGameChats(msg);
            this.props.navigation.navigate('Game', {gameId: msg,chats: gamechats});
        } else {
            if (!topic.includes("start")) {
                let newLobby = this.state.lobby;
                newLobby.users = msg;
                this.setState({lobby: newLobby});
            }
        }
    };


    render() {
        return (
            <ScrollView>
                <Text style={{fontSize: 20, margin: 20}}>{this.state.lobby.gameSettings._gameName}</Text>
                {this.state.lobby.users.map((user, i) => {
                    return (
                        <View key={i}>
                            <ListItem
                                key={i}
                                title={user.username}
                                titleStyle={{color: 'black', fontWeight: 'bold'}}
                                // leftAvatar={user.profilePicture}
                            />
                        </View>
                    )
                })}
                <Divider style={{backgroundColor: 'blue'}}/>
                <Text style={{fontSize: 20, margin: 20}}>Friends</Text>
                {this.state.friends.map((friend, i) => {
                    return (
                        <View key={i}>
                            <ListItem
                                key={i}
                                title={friend.username}
                                titleStyle={{color: 'black', fontWeight: 'bold'}}
                                subtitle={
                                    <Button title={"Invite"} onPress={() => this.inviteFriend(friend.email)}/>
                                }
                            />
                        </View>
                    )
                })}
                <SockJsClient url={this.state.serverUrl}
                              topics={['/lobby/' + this.state.lobby.lobbyId, '/lobby/start/' + this.state.lobby.lobbyId]}
                              onMessage={this.onMessageReceive} ref={(client) => {
                    this.clientRef = client
                }}
                              onConnect={() => {
                                  this.setState({clientConnected: true})
                              }}
                              onDisconnect={() => {
                                  this.setState({clientConnected: false})
                              }}
                              debug={true}
                />
                <Button onPress={() => this.handleStartGame()} title={"Start game"}>
                </Button>
            </ScrollView>
        )
    }
}
