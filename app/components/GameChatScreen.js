import React, {Component} from 'react';
import {View} from "react-native";
import SockJsClient from 'react-stomp';
import {GiftedChat} from "react-native-gifted-chat";
import Message from "../model/chat/Message";
import {GameChatMessage} from "../model/chat/ChatMessage";

export default class GameChatScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            currentChat: props.chat,
            serverUrl: props.serverUrl
        };

    }

    onMessageReceive = (msg, topic) => {
        this.setState((previousState) => {
            return {
                messages: GiftedChat.append(previousState.messages, {
                    _id: Math.round(Math.random() * 1000000),
                    text: msg.text,
                    user: {
                        _id: 'test',
                        name: 'React Native',
                    },
                }),
            };
        });
    };

    render() {
        return (
                <View style={{flex: 1,}}>
                <GiftedChat
                    messages={this.state.messages}
                    onSend={messages => this.onSend(messages)}
                    user={{
                        _id: 1,
                    }}
                />
                <SockJsClient url={this.state.serverUrl}
                              topics={['/gamechat/' + this.state.currentChat.id]}
                              onMessage={this.onMessageReceive} ref={(client) => {this.clientRef = client}}
                              onConnect={() => {this.setState({clientConnected: true})}}
                              onDisconnect={() => {this.setState({clientConnected: false})}}
                              debug={false}
                />
            </View>
        )
    }

    componentWillMount() {
        let messagesConverted = [];
        for (let message of this.state.currentChat.messages) {
            messagesConverted.push(new GameChatMessage(message.text))
        }
        this.setState({messages: messagesConverted.reverse()})
    }

    componentWillUnmount() {
    }

    onSend(messages = []) {
        const message: Message = new Message();
        message.text = messages[0].text;
        this.clientRef.sendMessage('/app/gamechat/send/message/' + this.state.currentChat.id, JSON.stringify(message));
    }


}
