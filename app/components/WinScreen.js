import React, {Component} from 'react';
import {StyleSheet, Text, View, Button, Image, ScrollView} from 'react-native';
import {getGameResults} from "../services/GameService";
import {GameStats} from "../model/game/GameStats";


export default class WinScreen extends Component {

  gameStats: GameStats;

  constructor(props) {
    super(props);
    this.state = {
      gameStats:  null,
    };
    getGameResults(props.navigation.getParam('gameId')).then((res) => {
      this.state.gameStats = res;
      this.setState(this.state);
    });
  }


  render() {
    if (this.state.gameStats == null || this.state.gameStats === undefined) return <Text>Loading...</Text>;

    return (
        <ScrollView contentContainerStyle={styles.container}>
          <View style={styles.winnerCardContainer}>
            <Text style={styles.textLabel}>Winning Role: { this.state.gameStats.winningRole }</Text>
            <Image
                style={styles.winnerCard}
                source={{uri: this.state.gameStats.winningRoleImg}}/>
          </View>
          <View styles={styles.phases}>
            <Text style={styles.textLabel}>Phases: { this.state.gameStats.roundCount }</Text>
          </View>
          <View style={styles.winners}>
            <Text style={styles.textLabel}>Winners:</Text>
            <Text>
              { this.state.gameStats.winners }
            </Text>
          </View>
          <View style={styles.kills}>
            <Text style={styles.textLabel}>Kills in order:</Text>
            <Text>
              { this.state.gameStats.killsInOrder }
            </Text>
          </View>

          <Button
            title={'Return to menu'}
            onPress={() => this.props.navigation.navigate('Menu')}/>
        </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  winnerCardContainer: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
  },
  winnerCard: {
    height: 400,
    width: 284,
    justifyContent: 'center',
    alignItems: 'center',
  },
  winners: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  kills: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textLabel: {
    fontWeight: 'bold',
    fontSize: 24,
  },
  phases: {
    flex: 0.5,
  }
});
