import React, {Component} from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    AsyncStorage,
    FlatList,
    Image,
    StyleSheet,
    TouchableWithoutFeedback,
    Animated
} from "react-native";
import Modal from "react-native-modal";
import GameChatScreen from "./GameChatScreen";
import SockJsClient from 'react-stomp';
import {Button} from "react-native-elements";
import {getGameObject, vote, getVoteResult, doSpecialVote, getRoles, getTarget} from "../services/GameService";
import * as mdl from "react-native-material-kit/lib/mdl";


export default class GameScreen extends Component {

    jwtToken = '';
    maxVotes = 0;
    bubbleCounter: number = 1;

    constructor(props) {
        super(props);
        this.state = {
            gameChats: props.navigation.getParam('chats'),
            isModalVisible: false,
            isTargetModalVisible: false,
            isVoteResultModalVisible: false,
            isGameInformationModalVisible: false,
            roles: [],
            selectedRole: '',
            showChatKeys: [],
            jwtToken: '',
            serverUrl: 'https://secret-stalin-backend.herokuapp.com/socket',
            game: null,
            voteResults: null,
            gameId: props.navigation.getParam('gameId'),
            participationIdVotedOn: -1,
            yPosition: new Animated.Value(0),
        };
        this.getToken().then(() => {
            let newServerUrl = this.state.serverUrl += '?access_token=' + this.jwtToken;
            this.setState({
                serverUrl: newServerUrl
            });
        });
        getGameObject(this.state.gameId).then((game) => {
            this.state.game = game;
            this.voteResults();
            if (!this.state.game.active) {
                this.props.navigation.navigate('WinScreen', {gameId: this.state.gameId});
            }
        });

        getRoles(this.state.gameId).then((roles) => {
            this.state.roles = roles;
        });
        this.getLastVotedOn();

    }

    getLastVotedOn() {
        getTarget(this.state.gameId, 'SLAV').then((result) => {
            this.setState({participationIdVotedOn: result})
        });
    }

    votePlayer(affectedId: number) {
        vote(this.state.game.gameId, affectedId).then(() => {
            this.getLastVotedOn();
        });
    }

    hideModal(key) {
        let array = [...this.state.showChatKeys]; // make a separate copy of the array
        let index = array.indexOf(key);
        if (index !== -1) {
            array.splice(index, 1);
            this.setState({showChatKeys: array});
        }
    }

    calculateVotes() {
        this.maxVotes = 0;
        if (this.state.voteResults !== undefined) {
            for (const c of this.state.voteResults) {
                this.maxVotes += c.voteCount;
            }
        }
    }

    voteResults() {
        getVoteResult(this.state.game.gameId).then((result) => {
            this.state.voteResults = result;
            this.calculateVotes();
            this.setState(this.state);
        });
    }

    selectTarget(gameId, targetParticipationId, role) {
        doSpecialVote(gameId, targetParticipationId, role).then((result) => {
        })
    }

    render() {
        if (this.state.game == null || this.state.voteResults == null) return <Text>Loading...</Text>;
        this.bubbleCounter = 1;

        return (
            <View style={[styles.container, {backgroundColor: this.state.game.state === "DAY" ? 'white' : 'black'}]}>
                <View style={styles.chat}>
                    {this.state.gameChats.map((chat, i) =>
                        <View key={chat.id}>
                            <Modal isVisible={this.state.showChatKeys.includes(i)}>
                                <TouchableOpacity
                                    style={styles.container}
                                    activeOpacity={1}
                                    onPress={() => this.hideModal(i)}>
                                    <TouchableWithoutFeedback>
                                        <GameChatScreen key={chat.id}
                                                        chat={chat}
                                                        serverUrl={this.state.serverUrl}
                                                        style={{flex: 1,}}/>
                                    </TouchableWithoutFeedback>
                                </TouchableOpacity>
                            </Modal>
                        </View>
                    )}

                    <View>
                        {this.state.roles.map(r => r.roleName).indexOf('HUNTER') >= 0 || this.state.roles.map(r => r.roleName).indexOf('MAYOR') >= 0 ?
                            <Modal isVisible={this.state.isTargetVisible}>
                                <TouchableOpacity
                                    style={styles.container}
                                    activeOpacity={1}
                                    onPress={() => this.setState({isTargetVisible: false})}>
                                    <View>
                                        <FlatList
                                            style={{flex: 1}}
                                            data={this.state.game.participationDTOs.filter(function (p) {
                                                return p.alive;
                                            })}
                                            renderItem={({item}) =>
                                                <Button title={item.userDTO.username}
                                                        onPress={() => this.selectTarget(this.state.gameId, item.participationId, this.state.selectedRole)}/>
                                            }
                                            numColumns={1}>
                                        </FlatList>
                                    </View>
                                </TouchableOpacity>
                            </Modal>
                            : null
                        }
                    </View>

                    <View>
                        <Modal isVisible={this.state.isModalVisible}>
                            <TouchableOpacity
                                style={styles.container}
                                activeOpacity={1}
                                onPress={() => this.setState({isModalVisible: false})}>
                                <TouchableWithoutFeedback>
                                    {this.state.voteResults == null || this.state.voteResults.length < 1 ?
                                        <Text style={{color: '#fff'}}>{'You can not vote this phase'}</Text> :
                                        <FlatList
                                            style={{flex: 1}}
                                            data={this.state.voteResults}
                                            renderItem={({item}) =>
                                                <View>
                                                    <Text style={{color: '#fff'}}> {item.username} </Text>
                                                    <mdl.Progress style={{width: 300, height: 50, flex: 1}}
                                                                  progress={item.voteCount / this.maxVotes}/>
                                                </View>
                                            }
                                            numColumns={1}>
                                        </FlatList>}
                                </TouchableWithoutFeedback>
                            </TouchableOpacity>
                        </Modal>
                    </View>

                    <FlatList
                        style={{flex: 1}}
                        data={this.state.game.participationDTOs}
                        renderItem={({item}) =>
                            <TouchableOpacity
                                onPress={() => this.votePlayer(item.participationId)}
                                style={[styles.card, {backgroundColor: item.alive ? item.colorCode : 'grey'}]}>
                                <Image
                                    style={styles.card_image}
                                    source={{uri: item.userDTO.profilePicture === '' ? 'https://material.angular.io/assets/img/examples/shiba1.jpg' : item.userDTO.profilePicture}}/>
                                {item.alive ? null : <Image
                                    style={styles.deceased}
                                    source={{uri: 'https://vignette.wikia.nocookie.net/dark-days-rpg/images/a/a9/Deceased.png/revision/latest?cb=20180509030125'}}/>}
                                {this.state.participationIdVotedOn !== item.participationId ? null : <Image
                                    style={styles.deceased}
                                    source={{uri: 'https://cdn.discordapp.com/attachments/389129246259281922/557135343627927555/Vote_stamp.png'}}/>}
                                <Text style={{color: '#fff'}}> {item.userDTO.username} </Text>
                            </TouchableOpacity>
                        }
                        numColumns={1}>
                    </FlatList>

                    <TouchableOpacity style={styles.expandButton}
                                      onPress={() => this.setState({isGameInformationModalVisible: true})}>
                        <Image style={{width: 35, height: 35, margin: 12}}
                               source={{uri: 'https://cdn4.iconfinder.com/data/icons/wirecons-free-vector-icons/32/add-128.png'}}/>
                    </TouchableOpacity>

                    <Modal isVisible={this.state.isGameInformationModalVisible}>
                        <TouchableOpacity
                            style={styles.container}
                            activeOpacity={1}
                            onPress={() => this.setState({isGameInformationModalVisible: false})}>
                            <TouchableWithoutFeedback>
                                <View style={[styles.container, {
                                    right: 9,
                                    bottom: 9 + 70 * this.bubbleCounter++,
                                    position: 'absolute'
                                }]}>
                                    <Text style={styles.bubbleText}>Show votes</Text>
                                    <TouchableOpacity style={[styles.expandBubble, {backgroundColor: '#0F9D58'}]}
                                                      onPress={() => this.setState({
                                                          isGameInformationModalVisible: false,
                                                          isModalVisible: true
                                                      })}>
                                        <Image style={{width: 35, height: 35, margin: 12}}
                                               source={{uri: 'https://image.flaticon.com/icons/png/512/61/61222.png'}}/>
                                    </TouchableOpacity>
                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback>
                                <View style={[styles.container, {
                                    right: 9,
                                    bottom: 9 + 70 * this.bubbleCounter++,
                                    position: 'absolute'
                                }]}>
                                    <Text style={styles.bubbleText}>Show my role cards</Text>
                                    <TouchableOpacity style={[styles.expandBubble, {backgroundColor: '#F4B400'}]}>
                                        <Image style={{width: 35, height: 35, margin: 12}}
                                               source={{uri: 'https://cdn.pixabay.com/photo/2016/04/22/14/31/info-1345871_960_720.png'}}/>
                                    </TouchableOpacity>
                                </View>
                            </TouchableWithoutFeedback>
                            {this.state.gameChats.map((chat, i) =>
                                <TouchableWithoutFeedback>
                                    <View style={[styles.container, {
                                        right: 9,
                                        bottom: 9 + 70 * this.bubbleCounter++,
                                        position: 'absolute'
                                    }]}>
                                        <Text style={styles.bubbleText}>{'Show ' + chat.name + ' chat'}</Text>
                                        <TouchableOpacity style={[styles.expandBubble, {backgroundColor: '#4285F4'}]}
                                                          onPress={() => this.setState({
                                                              isGameInformationModalVisible: false,
                                                              showChatKeys: [...this.state.showChatKeys, i]
                                                          })}>
                                            <Image style={{width: 35, height: 35, margin: 12}}
                                                   source={{uri: 'https://image.flaticon.com/icons/png/512/151/151808.png'}}/>
                                        </TouchableOpacity>
                                    </View>
                                </TouchableWithoutFeedback>
                            )}
                            {this.state.roles.map(r => r.roleName).indexOf('HUNTER') >= 0 ?
                                <TouchableWithoutFeedback>
                                    <View style={[styles.container, {
                                        right: 9,
                                        bottom: 9 + 70 * this.bubbleCounter++,
                                        position: 'absolute'
                                    }]}>
                                        <Text style={styles.bubbleText}>{'Select Target'}</Text>
                                        <TouchableOpacity style={[styles.expandBubble, {backgroundColor: '#ff000c'}]}
                                                          onPress={() => this.setState({
                                                              isTargetVisible: true,
                                                              selectedRole: 'HUNTER'
                                                          })}>
                                            <Image style={{width: 35, height: 35, margin: 12}}
                                                   source={{uri: 'https://icons-for-free.com/free-icons/png/512/2538764.png'}}/>
                                        </TouchableOpacity>
                                    </View>
                                </TouchableWithoutFeedback>

                                : null
                            }
                            {this.state.roles.map(r => r.roleName).indexOf('MAYOR') >= 0 ?
                                <TouchableWithoutFeedback>
                                    <View style={[styles.container, {
                                        right: 9,
                                        bottom: 9 + 70 * this.bubbleCounter++,
                                        position: 'absolute'
                                    }]}>
                                        <Text style={styles.bubbleText}>{'Select Successor'}</Text>
                                        <TouchableOpacity style={[styles.expandBubble, {backgroundColor: '#ff000c'}]}
                                                          onPress={() => this.setState({
                                                              isTargetVisible: true,
                                                              selectedRole: 'MAYOR'
                                                          })}>
                                            <Image style={{width: 35, height: 35, margin: 12}}
                                                   source={{uri: 'https://www.shareicon.net/data/512x512/2015/09/29/648483_vote_512x512.png'}}/>
                                        </TouchableOpacity>
                                    </View>
                                </TouchableWithoutFeedback>

                                : null
                            }
                        </TouchableOpacity>
                    </Modal>
                </View>
                <SockJsClient url={this.state.serverUrl}
                              topics={['/game/' + this.state.game.gameId]}
                              onMessage={(msg) => {
                                  if (msg !== undefined) {
                                      getGameObject(this.state.game.gameId).then((game) => {
                                          if (game.active) {
                                              this.setState({game: game, participationIdVotedOn: -1});
                                          } else {
                                              this.props.navigation.navigate('WinScreen', {gameId: this.state.gameId});
                                          }
                                      });
                                  }
                              }}
                              onConnect={() => {
                                  this.setState({clientConnected: true})
                              }}
                              onDisconnect={() => {
                                  this.setState({clientConnected: false})
                              }}
                              debug={true}
                />
                <SockJsClient url={this.state.serverUrl}
                              topics={['/game/votes/' + this.state.game.gameId]}
                              onMessage={(msg) => {
                                  if (msg !== undefined) {
                                      this.voteResults();
                                  }
                              }}
                              onConnect={() => {
                                  this.setState({clientConnected: true})
                              }}
                              onDisconnect={() => {
                                  this.setState({clientConnected: false})
                              }}
                              debug={true}
                />

            </View>
        )
    }

    async getToken() {
        await AsyncStorage.getItem('jwt').then(value => {
            if (value !== null) {
                this.jwtToken = value;
            } else {

            }
        });
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    chatButtons: {
        flex: 1,
        alignItems: 'center',
        borderStyle: 'solid',
        borderWidth: 2,
        borderRadius: 4,
        padding: 10,
        margin: 5,
        backgroundColor: 'black',
        justifyContent: 'space-evenly',
    },
    chat: {
        flex: 1,
        flexDirection: 'column',
        margin: 10,
    },
    game: {
        flex: 5,
        flexDirection: 'column',
    },
    card: {
        flex: 1,
        alignItems: 'center',
        borderStyle: 'solid',
        borderWidth: 2,
        borderRadius: 4,
        padding: 10,
        margin: 5,
    },
    card_image: {
        height: 50,
        width: 50,
        borderRadius: 10,
        overflow: 'hidden',
        alignContent: 'center',
        margin: 5,
    },
    deceased: {
        position: 'absolute',
        width: 250,
        height: 100,
        bottom: 0,
    },
    expandButton: {
        position: 'absolute',
        height: 60,
        width: 60,
        borderRadius: 30,
        backgroundColor: '#DB4437',
        bottom: 20,
        right: 20,
        elevation: 3
    },
    expandBubble: {
        height: 60,
        width: 60,
        borderRadius: 30,
    },
    bubbleText: {
        marginRight: 25,
        padding: 10,
        color: 'black',
        backgroundColor: 'white',
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
});

