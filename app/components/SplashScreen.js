import React, {Component} from 'react';
import {AsyncStorage, StyleSheet, Text, View} from "react-native";

type Props = {};

export default class SplashScreen extends Component<Props> {
    constructor(props) {
        super(props);
        this.authenticateSession();
    }

    authenticateSession() {
        AsyncStorage.getItem('jwt').then(value => {
            if (value !== null) {
                this.props.navigation.navigate('Menu');
            } else {
                this.props.navigation.navigate('Login');
            }
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.header}>Splash Screen</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 40,
        marginBottom: 50,
        fontWeight: 'bold',
    },
    label: {
        fontSize: 24,
        marginBottom: 5,
        marginTop: 10,
        flexDirection: 'column',
        alignItems: 'flex-start',
    },
    input: {
        width: 350,
        height: 55,
        backgroundColor: '#42A5F5',
        margin: 10,
        padding: 8,
        color: 'white',
        borderRadius: 14,
        fontSize: 18,
        fontWeight: '500',
    },
    container: {
        justifyContent: 'center',
        marginTop: 50,
        padding: 20,
        backgroundColor: '#ffffff',
    }
});
