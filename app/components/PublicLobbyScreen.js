import React, {Component} from 'react';
import {AsyncStorage, ScrollView} from "react-native";
import {getJoinedUsers, getPublicLobbies, joinLobby} from "../services/LobbyService";
import {Card, ListItem} from "react-native-elements";
import TouchableScale from 'react-native-touchable-scale';
import {Icon} from 'react-native-elements'
import {Lobby} from "../model/lobby/Lobby";
import * as Alert from "react-native/Libraries/Alert/Alert";


export default class PublicLobbyScreen extends Component {

    jwtToken = '';

    constructor(props) {
        super(props);
        this.state = {
            lobbies: []
        };
        this.getToken().then();
        getPublicLobbies().then(lobbiesResult => {
            this.setState({lobbies: lobbiesResult})
        })
    }

    async getToken() {
        await AsyncStorage.getItem('jwt').then(value => {
            if (value !== null) {
                this.jwtToken = value;
            } else {

            }
        });
    }

    joinLobbyNavigate(lobbyInput: Lobby) {
        joinLobby(lobbyInput.lobbyId).then(res => {
            if (res == null) {
                getJoinedUsers(lobbyInput.lobbyId).then(users => {
                    lobbyInput.users = users;
                    this.props.navigation.navigate('Lobby', {lobby: lobbyInput, jwtToken: this.jwtToken});
                });
            } else {
                Alert.alert(res)
            }
        })
    }

    render() {
        return (
            <ScrollView>
                <Card title="Public lobbies">
                    {
                        this.state.lobbies.map((lobby, i) => {
                            return (
                                <ListItem
                                    key={i}
                                    onPress={() => this.joinLobbyNavigate(lobby)}
                                    Component={TouchableScale}
                                    friction={90}
                                    tension={100}
                                    activeScale={0.95}
                                    title={lobby.gameSettings._gameName}
                                    titleStyle={{color: 'black', fontWeight: 'bold'}}
                                    subtitleStyle={{color: 'black'}}
                                    subtitle={"Max players: " + lobby.gameSettings._maxPlayers}
                                    chevron={<Icon
                                        name="chevron-right"/>}
                                />
                            );
                        })
                    }
                </Card>
            </ScrollView>
        )
    }
}
