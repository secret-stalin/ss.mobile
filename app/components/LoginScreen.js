import React, {Component} from 'react';
import {StyleSheet, Text, View, Button, Alert, AsyncStorage} from 'react-native';
import t from 'tcomb-form-native';
import {loginUser} from '../services/Auth';
import firebase from "react-native-firebase";
import {setFireBaseToken} from "../services/UserService";

const Form = t.form.Form;

const UserLogin = t.struct({
    email: t.String,
    password: t.String,
});

const options = {
    fields: {
        email: {
            error: "Email doesn't exist in our database",
            autoCapitalize: 'none',
        },
        password: {
            secureTextEntry: true,
            error: "Password not correct",
        }
    }
};

export default class LoginScreen extends Component {
    async handleSubmit() {
        const value = this.refs.form.getValue(); // use that ref to get the form value
        if (value) {
            await loginUser(value.email, value.password);
            if (await AsyncStorage.getItem('jwt') !== null) {
                let fcmToken;
                fcmToken = await firebase.messaging().getToken();
                await setFireBaseToken(fcmToken);
                this.props.navigation.navigate('Menu');
            }
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.header}>Login</Text>
                <Form
                    ref="form" // assign a ref
                    type={UserLogin}
                    options={options}
                />
                <Button
                    title="Login!"
                    onPress={this.handleSubmit.bind(this)}
                />
                <Button
                    title="No account yet? Register here!"
                    onPress={() => this.props.navigation.navigate('Register')}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 40,
        marginBottom: 50,
        fontWeight: 'bold',
    },
    label: {
        fontSize: 24,
        marginBottom: 5,
        marginTop: 10,
        flexDirection: 'column',
        alignItems: 'flex-start',
    },
    input: {
        width: 350,
        height: 55,
        backgroundColor: '#42A5F5',
        margin: 10,
        padding: 8,
        color: 'white',
        borderRadius: 14,
        fontSize: 18,
        fontWeight: '500',
    },
    container: {
        justifyContent: 'center',
        marginTop: 50,
        padding: 20,
        backgroundColor: '#ffffff',
    }
});
