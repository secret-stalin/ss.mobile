import React, {Component} from 'react';
import {AsyncStorage, Button, ScrollView, StyleSheet, View} from "react-native";
import {Settings} from "../model/lobby/GameSettings";

import t from 'tcomb-form-native';
import {getGame, makeGame} from "../services/GameService";
import {Lobby} from "../model/lobby/Lobby";

const Form = t.form.Form;

let Role = t.enums({
    Hunter: 'Hunter',
    Mayor: 'Mayor'
});

const GameSettings = t.struct({
    _gameId: t.maybe(t.Number),
    _gameName: t.String,
    _maxPlayers: t.Number,
    _isPrivate: t.Boolean,
    _dayTime: t.Number,
    _nightTime: t.Number,
    _roles: t.list(Role)
});

const formStyles = {
    ...Form.stylesheet,
    formGroup: {
        normal: {
            marginBottom: 10
        },
    },
    controlLabel: {
        normal: {
            color: 'blue',
            fontSize: 18,
            marginBottom: 7,
            fontWeight: '600'
        },
        // the style applied when a validation error occours
        error: {
            color: 'red',
            fontSize: 18,
            marginBottom: 7,
            fontWeight: '600'
        }
    }
};

const options = {
    fields: {
        _gameId: {
            //label: 'Game Id',
            hidden: true,
        },
        _gameName: {
            label:'Game Name',
        },
        _maxPlayers: {
            label: 'Max amount of players',
        },
        _isPrivate: {
            label: 'Private game?',
        },
        _dayTime: {
            label: 'Day time',
        },
        _nightTime: {
            label: 'Night time'
        },
        _roles: {
            hidden: true,
        }
    },
    stylesheet: formStyles,
};

export default class CreateGameScreen extends Component {


    jwtToken = '';
    constructor(props) {
        super(props);
        this.state = {
            settings: new Settings(),
            lobbyId: 0
        };
        this.getToken().then();
        getGame().then(settingsResponse => {
            this.setState({settings: settingsResponse});
        });
    }

    async getToken() {
        await AsyncStorage.getItem('jwt').then(value => {
            if (value !== null) {
                this.jwtToken = value;
            } else {

            }
        });
    }

    handleSubmit = async () => {
        if (this._form.getValue() != null) {
            const value = this._form.getValue();
            let gameSettings = new Settings();
            gameSettings.gameName = value._gameName;
            gameSettings.maxPlayers = value._maxPlayers;
            gameSettings.isPrivate = value._isPrivate;
            gameSettings.dayTime = value._dayTime;
            gameSettings.nightTime = value._nightTime;
            gameSettings.roles = value._roles;
            gameSettings.gameId = this.state.settings.gameId;
            await makeGame(gameSettings).then(lobbyRes => {
                this.props.navigation.navigate('Lobby', {lobby: lobbyRes, jwtToken: this.jwtToken});
            });
        }
    };

    render() {
        return (
            <ScrollView>
                <View style={styles.container}>
                    <Form
                        ref={c => this._form = c}
                        type={GameSettings}
                        options={options}
                    />
                </View>
                <Button
                    title="Create Game"
                    onPress={this.handleSubmit}
                />
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        marginTop: 50,
        padding: 20,
        backgroundColor: '#ffffff',
    },
});

