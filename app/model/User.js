export class User {
    username: string;
    status: string;
    friends: User[];
    email: string;
    oldPassword: string;
    newPassword: string;
    profilePicture: string;

    constructor() {
        this.username = '';
        this.status = '';
        this.friends = [];
        this.email = '';
        this.oldPassword = '';
        this.newPassword = '';
        this.profilePicture = '';
    }
}
