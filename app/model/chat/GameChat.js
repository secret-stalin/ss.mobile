import Message from "./Message";

export class GameChat {
    id: number;
    name: string;
    messages: Message[];

    constructor() {
        this.id = 0;
        this.name = '';
        this.messages = [];
    }
}
