export class GameChatMessage {
    _id: number;
    text: 'Hello developer';
    user;

    constructor(text: string) {
        this._id = Math.round(Math.random() * 1000000);
        this.text = text;
        this.user = {
            _id: 'test',
            name: 'React Native',
            avatar: 'https://placeimg.com/140/140/any',
        };
    }
}
