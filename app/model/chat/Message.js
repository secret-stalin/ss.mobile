export default class Message {
    text: string;
    timestamp: string;

    constructor() {
        this.text = '';
        this.timestamp = '';
    }
}
