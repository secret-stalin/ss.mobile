import {User} from "../User";
import {Settings} from "./GameSettings";

export class Lobby {
    lobbyId: number;
    users: User[];
    gameSettings: Settings;

    constructor() {
        this.users = [];
        this.gameSettings = new Settings();
    }
}
