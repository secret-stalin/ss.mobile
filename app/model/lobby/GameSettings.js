export class Settings {
    _gameId: number;
    _gameName: string;
    _maxPlayers: number;
    _isPrivate: boolean;
    _dayTime: string;
    _nightTime: string;
    _roles: string[];


    constructor() {
        this._gameId = 0;
        this._gameName = '';
        this._maxPlayers = 0;
        this._isPrivate = false;
        this._dayTime = '';
        this._nightTime = '';
        this._roles = [];
    }


    get gameId() {
        return this._gameId;
    }

    set gameId(value) {
        this._gameId = value;
    }

    get gameName() {
        return this._gameName;
    }

    set gameName(value) {
        this._gameName = value;
    }

    get maxPlayers() {
        return this._maxPlayers;
    }

    set maxPlayers(value) {
        this._maxPlayers = value;
    }

    get isPrivate() {
        return this._isPrivate;
    }

    set isPrivate(value) {
        this._isPrivate = value;
    }

    get dayTime() {
        return this._dayTime;
    }

    set dayTime(value) {
        this._dayTime = value;
    }

    get nightTime() {
        return this._nightTime;
    }

    set nightTime(value) {
        this._nightTime = value;
    }

    get roles() {
        return this._roles;
    }

    set roles(value) {
        this._roles = value;
    }
}
