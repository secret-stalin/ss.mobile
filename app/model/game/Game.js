import {Participation} from "./Participation";

export class Game {
    gameId: number;
    name: string;
    roleTypeToVote: string;
    state: string;
    participations: Participation[];
    active: boolean;
    winningRole: string;

    constructor() {
        this.gameId = 0;
        this.name = '';
        this.roleTypeToVote = '';
        this.state = '';
        this.participations = [];
        this.active = false;
        this.winningRole = '';
    }
}
