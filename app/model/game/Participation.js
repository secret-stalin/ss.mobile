import {User} from "../User";


export class Participation {
    participationId: number;
    alive: boolean;
    colorCode: string;
    userDTO: User;


    constructor() {
        this.participationId = 0;
        this.alive = true;
        this.colorCode = '';
        this.userDTO = new UserDTO();
    }
}
