export class GameStats{
  winningRole: string;
  winningRoleImg: string;
  winners: string[];
  roundCount: number;
  killsInOrder: string[];

  constructor() {
    this.winningRole = '';
    this.winningRoleImg = '';
    this.winners = [];
    this.roundCount = 0;
    this.killsInOrder = [];
  }
}
