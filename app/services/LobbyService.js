import {AsyncStorage} from "react-native";
import {Lobby} from "../model/lobby/Lobby";
import {User} from "../model/User";

const lobbyPublicUrl = 'https://secret-stalin-backend.herokuapp.com' + '/api/lobby/public/';
const getJoinedUsersUrl = 'https://secret-stalin-backend.herokuapp.com' + '/api/lobby/joined/';
const inviteUrl = 'https://secret-stalin-backend.herokuapp.com' + '/api/lobby/';

export async function getPublicLobbies(): Lobby[] {

    let jwtToken = '';
    await AsyncStorage.getItem('jwt').then(value => {
        if (value !== null) {
            jwtToken = value;
        } else {
            jwtToken = 'None here';
        }
    });
    let response = await fetch(
        lobbyPublicUrl,
        {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + jwtToken,
            }
        }
    );
    let responseJson = await response.json();

    if (response.status === 200) {
        let lobbies: Lobby = responseJson;
        return lobbies;
    } else {
        console.log(response.status + " response status error");
        console.log(responseJson.error_description + " response status desc");
    }
}


export async function joinLobby(lobbyId: number): string {

    let jwtToken = '';
    await AsyncStorage.getItem('jwt').then(value => {
        if (value !== null) {
            jwtToken = value;
        } else {
            jwtToken = 'None here';
        }
    });
    let response = await fetch(
        lobbyPublicUrl + lobbyId,
        {
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + jwtToken,
            }
        }
    );

    if (response.status === 200) {
        return null;
    } else {
        console.log(response.status + " response status error");
        return "";
    }
}

export async function getJoinedUsers(lobbyId: number): User[] {
    let jwtToken = '';
    await AsyncStorage.getItem('jwt').then(value => {
        if (value !== null) {
            jwtToken = value;
        } else {
            jwtToken = 'None here';
        }
    });
    let response = await fetch(
        getJoinedUsersUrl + lobbyId,
        {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + jwtToken,
            }
        }
    );
    let responseJson = await response.json();

    if (response.status === 200) {
        let users: User[] = responseJson;
        return users;
    } else {
        console.log(response.status + " response status error");
        console.log(responseJson.error_description + " response status desc");
    }
}

export async function sendInvite(lobbyId: number, email: string): string {

    let jwtToken = '';
    await AsyncStorage.getItem('jwt').then(value => {
        if (value !== null) {
            jwtToken = value;
        } else {
            jwtToken = 'None here';
        }
    });
    let response = await fetch(
        inviteUrl + lobbyId + '/invite/' + email,
        {
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + jwtToken,
            }
        }
    );

    if (response.status === 200) {
        return null;
    } else {
        console.log(response.status + " response status error");
        return "nooooo";
    }
}
