import {AsyncStorage} from "react-native";
import jwt_decode from 'jwt-decode';
import {User} from "../model/User";

const userUrl = 'https://secret-stalin-backend.herokuapp.com' + '/api/user/';
const setTokenUrl = 'https://secret-stalin-backend.herokuapp.com' + '/api/user/fcmtoken/';

export async function decodeJwt(): string {
    let jwtToken = '';
    await AsyncStorage.getItem('jwt').then(value => {
        if (value !== null) {
            jwtToken = value;
        } else {
            jwtToken = 'None here';
        }
    });
    let decoded = jwt_decode(jwtToken);
    return decoded;
}

export async function getUserFriends(email: string): User[] {
    let jwtToken = '';
    await AsyncStorage.getItem('jwt').then(value => {
        if (value !== null) {
            jwtToken = value;
        } else {
            jwtToken = 'None here';
        }
    });
    let response = await fetch(
        userUrl + email,
        {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + jwtToken,
            }
        }
    );
    let responseJson = await response.json();

    if (response.status === 200) {
        let friends: User[] = responseJson;
        return friends;
    } else {
        console.log(response.status + " response status error");
    }
}

export async function setFireBaseToken(firebasetoken: string): string {
    let jwtToken = '';
    await AsyncStorage.getItem('jwt').then(value => {
        if (value !== null) {
            jwtToken = value;
        } else {
            jwtToken = 'None here';
        }
    });

    let response = await fetch(
        setTokenUrl + firebasetoken,
        {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + jwtToken,
            }
        }
    );
    let responseJson = await response;

    if (response.status === 200) {
        return "";
    } else {
        console.log(response.status + " response status error");
        return "";
    }
}
