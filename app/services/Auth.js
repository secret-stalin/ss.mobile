import {Alert, AsyncStorage} from 'react-native'

export async function loginUser(username: String, password: String): string {
    try {
        let response = await fetch(
            'https://secret-stalin-backend.herokuapp.com/oauth/token',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': 'Basic dGVzdGp3dGNsaWVudGlkOlhZN2ttem9OemwxMDA=',
                },
                body: "username=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password) + "&grant_type=password"
            }
        );
        let responseJson = await response.json();

        if (response.status === 200) {
            await AsyncStorage.setItem('jwt', responseJson.access_token);
        } else {
            Alert.alert(responseJson.error_description);
        }
    } catch (e) {
        return null;
    }
}

export async function getJwtToken(): string {
    await AsyncStorage.getItem('jwt').then(value => {
        if (value !== null) {
            return value;
        } else {
            return '';
        }
    });
}

