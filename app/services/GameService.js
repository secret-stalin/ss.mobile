import {GameChat} from "../model/chat/GameChat";
import {Alert, AsyncStorage} from "react-native";
import {Settings} from "../model/lobby/GameSettings";
import {Lobby} from "../model/lobby/Lobby";
import {Game} from "../model/game/Game";

const gameUrl = 'https://secret-stalin-backend.herokuapp.com' + '/api/game/';
const gamesUrl = 'https://secret-stalin-backend.herokuapp.com' + '/api/games/active/';
const lobbyUrl = 'https://secret-stalin-backend.herokuapp.com' + '/api/lobby/';
const lobbyStartUrl = 'https://secret-stalin-backend.herokuapp.com' + '/api/lobby/start/';
const voteUrl = 'https://secret-stalin-backend.herokuapp.com' + '/api/vote';
const voteResultUrl = 'https://secret-stalin-backend.herokuapp.com' + '/api/voteResults/';
const gameStatsUrl = 'https://secret-stalin-backend.herokuapp.com' + '/api/game/stats/';
const rolesUrl =  'https://secret-stalin-backend.herokuapp.com' + '/api/game/my-roles/'

export async function getGame(): GameChat[] {

    let jwtToken = '';
    await AsyncStorage.getItem('jwt').then(value => {
        if (value !== null) {
            jwtToken = value;
        } else {
            jwtToken = 'None here';
        }
    });

    let response = await fetch(
        gameUrl,
        {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + jwtToken,
            }
        }
    );
    let responseJson = await response.json();

    if (response.status === 201) {
        let gamesettings: Settings = responseJson;
        return gamesettings;
    } else {
        console.log(response.status + " response status error");
        console.log(responseJson.error_description + " response status desc");
    }
}

export async function makeGame(settings: Settings): Lobby {
    try {
        let jwtToken = '';
        await AsyncStorage.getItem('jwt').then(value => {
            if (value !== null) {
                jwtToken = value;
            } else {
                jwtToken = 'None here';
            }
        });

        let response = await fetch(
            lobbyUrl + settings.gameId,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + jwtToken,
                },
                body: JSON.stringify(settings)
            }
        );
        let responseJson = await response.json();

        if (response.status === 200) {
            return responseJson;
        } else {
            console.log(response.status + " response status error");
            console.log(responseJson.error_description + " response status desc");
        }
    } catch (e) {
        return null;
    }
}

export async function startGame(lobbyId: number): Game {
    try {
        let jwtToken = '';
        await AsyncStorage.getItem('jwt').then(value => {
            if (value !== null) {
                jwtToken = value;
            } else {
                jwtToken = 'None here';
            }
        });

        let response = await fetch(
            lobbyStartUrl + lobbyId,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + jwtToken,
                }
            }
        );
        let responseJson = await response.json();

        if (response.status === 200) {
            return responseJson;
        } else {
            console.log(response.status + " response status error");
            console.log(responseJson.error_description + " response status desc");
        }
    } catch (e) {
        return null;
    }
}

export async function getGameObject(gameId: number): any {
    try {
        let response = await fetch(
            gameUrl + gameId,
            {
                method: 'GET'
            }
        );
        let responseJson = await response.json();
        return responseJson;
    } catch (e) {
        Alert.alert(e.message);
    }
}

export async function getRoles(gameId: number) {
    try {
        let jwtToken = '';
        await AsyncStorage.getItem('jwt').then(value => {
            if (value !== null) {
                jwtToken = value;
            } else {
                jwtToken = 'None here';
            }
        });

        let response = await fetch(
            rolesUrl + gameId,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + jwtToken,
                }
            }
        );
        let responseJson = await response.json();
        return responseJson;
    } catch (e) {
        Alert.alert(e.message);
    }
}

export async function doSpecialVote(gameId: number, targetparticipationId: number, role: string) {
    try {
        let jwtToken = '';
        await AsyncStorage.getItem('jwt').then(value => {
            if (value !== null) {
                jwtToken = value;
            } else {
                jwtToken = 'None here';
            }
        });

        let response = await fetch(
            voteUrl,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization':'Bearer ' + jwtToken,
                },
                method: 'POST',
                body: JSON.stringify({gameId: gameId, affectedPlayerId: targetparticipationId, roleTypeExecutor: role})
            }
        );
        //let responseJson = await response.json();
    } catch (e) {
        Alert.alert(e.message);
    }
}

export async function vote(gameId: number, affectedPlayerId: number) {
    try {
        let jwtToken = '';
        await AsyncStorage.getItem('jwt').then(value => {
            if (value !== null) {
                jwtToken = value;
            } else {
                jwtToken = 'None here';
            }
        });

        let response = await fetch(
            voteUrl,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization':'Bearer ' + jwtToken,
                },
                method: 'POST',
                body: JSON.stringify({gameId: gameId, affectedPlayerId: affectedPlayerId})
            }
        );
        //let responseJson = await response.json();
    } catch (e) {
        Alert.alert(e.message);
    }
}

export async function getVoteResult(gameId: number): any {
    try {
        let jwtToken = '';
        await AsyncStorage.getItem('jwt').then(value => {
            if (value !== null) {
                jwtToken = value;
            } else {
                jwtToken = 'None here';
            }
        });

        let response = await fetch(
            voteResultUrl + gameId,
            {
                headers: {
                    'Authorization': 'Bearer ' + jwtToken,
                },
                method: 'GET'
            }
        );
        let responseJson = await response.json();
        return responseJson;
    } catch (e) {
        Alert.alert(e.message);
    }
}

export async function getGameResults(gameId: number): any {
    try {
        let jwtToken = '';
        await AsyncStorage.getItem('jwt').then(value => {
            if (value !== null) {
                jwtToken = value;
            } else {
                jwtToken = 'None here';
            }
        });

        let response = await fetch(
            gameStatsUrl + gameId,
            {
                headers: {
                    'Authorization': 'Bearer ' + jwtToken,
                },
                method: 'GET'
            }
        );
        let responseJson = await response.json();
        return responseJson;
    } catch (e) {

    }
}

export async function getGames(): Game[] {

    let jwtToken = '';
    await AsyncStorage.getItem('jwt').then(value => {
        if (value !== null) {
            jwtToken = value;
        } else {
            jwtToken = 'None here';
        }
    });

    let response = await fetch(
        gamesUrl,
        {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + jwtToken,
            }
        }
    );
    let responseJson = await response.json();

    if (response.status === 200) {
        let games: Game[] = responseJson;
        return games;
    } else {
        console.log(response.status + " response status error");
        console.log(responseJson.error_description + " response status desc");
    }
}

export async function getTarget(gameId: number, selectedRoleAction: string): any {
    let jwtToken = '';
    await AsyncStorage.getItem('jwt').then(value => {
        if (value !== null) {
            jwtToken = value;
        } else {
            jwtToken = 'None here';
        }
    });

    try {
        let response = await fetch(
            gameUrl + gameId + '/target/' + selectedRoleAction,
            {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + jwtToken,
                }
            }
        );
        let responseJson = await response.json();
        if (response.status === 200) {
            let id = responseJson;
            return id;
        } else {
            console.log(response.status + " response status error");
            console.log(responseJson.error_description + " response status desc");
        }
    } catch (e) {
        return -1;
    }
}



