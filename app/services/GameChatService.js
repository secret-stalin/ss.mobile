import {AsyncStorage} from "react-native";
import {GameChat} from "../model/chat/GameChat";

export async function getGameChats(gameId: number): GameChat[] {
    let jwtToken = '';
    await AsyncStorage.getItem('jwt').then(value => {
        if (value !== null) {
            jwtToken = value;
        } else {
            jwtToken = 'None here';
        }
    });
    let response = await fetch(
        'https://secret-stalin-backend.herokuapp.com/api/game/chats/' + gameId,
        {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + jwtToken
            }
        }
    );
    let responseJson = await response.json();

    if (response.status === 200) {
        let gameChats: GameChat[] = responseJson;
        return gameChats;
    } else {
        console.log(response.status + " response status error");
    }
}
